var Glitcher = (function () {
    function Glitcher(options) {
        this.canvas = document.createElement('canvas');
        this.context = this.canvas.getContext('2d');
        this.origCanvas = document.createElement('canvas');
        this.origContext = this.origCanvas.getContext('2d');
        this.options = options;
    }
    Glitcher.prototype.glitch = function (url, callback) {
        var _this = this;
        this.loadImage(url, function (img) {
            _this.renderImage(img);
            _this.process();
            callback();
        });
    };

    Glitcher.prototype.process = function () {
        var imageData = this.origContext.getImageData(0, 0, this.width, this.height), pixels = imageData.data, length = pixels.length, options = this.options, brightness, offset, i, x, y;

        for (i = 0; i < length; i += 4) {
            if (options.color) {
                pixels[i] *= options.color.red;
                pixels[i + 1] *= options.color.green;
                pixels[i + 2] *= options.color.blue;
            }

            if (options.greyscale) {
                brightness = pixels[i] * options.greyscale.red + pixels[i + 1] * options.greyscale.green + pixels[i + 2] * options.greyscale.blue;

                pixels[i] = brightness;
                pixels[i + 1] = brightness;
                pixels[i + 2] = brightness;
            }

            if (options.stereoscopic) {
                offset = options.stereoscopic.red;
                pixels[i] = (pixels[i + 4 * offset] === undefined) ? 0 : pixels[i + 4 * offset];

                offset = options.stereoscopic.green;
                pixels[i + 1] = (pixels[i + 1 + 4 * offset] === undefined) ? 0 : pixels[i + 1 + 4 * offset];

                offset = options.stereoscopic.blue;
                pixels[i + 2] = (pixels[i + 2 + 4 * offset] === undefined) ? 0 : pixels[i + 2 + 4 * offset];
            }
        }

        if (options.lineOffset) {
            i = 0;

            for (y = 0; y < this.height; y++) {
                offset = (y % options.lineOffset.lineHeight === 0) ? Math.round(Math.random() * options.lineOffset.value) : offset;

                for (x = 0; x < this.width; x++) {
                    i += 4;
                    pixels[i + 0] = (pixels[i + 4 * offset] === undefined) ? 0 : pixels[i + 4 * offset];
                    pixels[i + 1] = (pixels[i + 1 + 4 * offset] === undefined) ? 0 : pixels[i + 1 + 4 * offset];
                    pixels[i + 2] = (pixels[i + 2 + 4 * offset] === undefined) ? 0 : pixels[i + 2 + 4 * offset];
                }
            }
        }

        if (options.glitch) {
        }

        this.context.putImageData(imageData, 0, 0);
    };

    Glitcher.prototype.loadImage = function (url, callback) {
        var img = document.createElement('img');
        img.crossOrigin = 'anonymous';
        img.onload = function () {
            callback(img);
        };
        img.src = url;
    };

    Glitcher.prototype.renderImage = function (img) {
        this.canvas.width = this.origCanvas.width = this.width = img.width;
        this.canvas.height = this.origCanvas.height = this.height = img.height;

        this.origContext.drawImage(img, 0, 0);
    };
    return Glitcher;
})();

var glitcher = new Glitcher({
    color: {

    },
    stereoscopic: {
        red: 10,
        green: 20,
        blue: 0
    },
    lineOffset: {
        value: 4
    }
});

var glitcher2 = new Glitcher({
    color: {

    },
    stereoscopic: {
        red: 10,
        green: 20,
        blue: 0
    },
    lineOffset: {
        value: 4
    }
});

var glitcher3 = new Glitcher({
    color: {

    },
    stereoscopic: {
        red: 10,
        green: 20,
        blue: 0
    },
    lineOffset: {
        value: 4
    }
});

glitcher.glitch('https://fbcdn-sphotos-e-a.akamaihd.net/hphotos-ak-xla1/v/t1.0-9/13872969_1451463591545918_2139046000440846354_n.jpg?oh=34c64ae6263695fb116401b6040331fc&oe=58594182&__gda__=1478082892_eca84bb8751319d76cae7a7864fd1424', function () {
    document.getElementById("lina-lopes").appendChild(glitcher.canvas);
});

glitcher2.glitch('https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xlp1/v/t1.0-9/13876442_1451468724878738_4810616473709042472_n.jpg?oh=b30fce88da9ad9ddfb0fee5ff1d75275&oe=585A6017&__gda__=1477865218_c57afb5e3655287f797089e2ce41a067', function () {
    document.getElementById("lilo-kids").appendChild(glitcher2.canvas);
});

glitcher3.glitch('https://scontent-grt2-1.xx.fbcdn.net/v/t1.0-9/13934990_1452221324803478_154237044191732338_n.jpg?oh=7ea92fb5085b9cfad2da5a01840acd92&oe=581FD8A7', function () {
    document.getElementById("lilo-me").appendChild(glitcher3.canvas);
});

function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

setInterval(function () {
    glitcher.options = {
        color: {

        },
        stereoscopic: {
            red: 1,
            green: 1,
            blue: 30 * randomRange(1, 3)
        },
        lineOffset: {
            value: 5 * randomRange(1, 3),
            lineHeight: 10 * randomRange(1, 3)
        }
    };
    glitcher.process();


        glitcher2.options = {
        color: {

        },
        stereoscopic: {
            red: 1,
            green: 1,
            blue: 30 * randomRange(1, 3)
        },
        lineOffset: {
            value: 5 * randomRange(1, 3),
            lineHeight: 10 * randomRange(1, 3)
        }
    };
   glitcher2.process();
        glitcher3.options = {
        color: {

        },
        stereoscopic: {
            red: 1,
            green: 1,
            blue: 30 * randomRange(1, 3)
        },
        lineOffset: {
            value: 5 * randomRange(1, 3),
            lineHeight: 10 * randomRange(1, 3)
        }
    };
   glitcher3.process();

    
 
}, 50);